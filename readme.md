# Kata - A cat image downloader

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [License](#license)

## Requirements
* Requires `gcc`, `cmake`, `nlohmann_json`, `restclient-cpp`.

## Usage
* mkdir build
* cd build
* cmake ../
* make
* ./cats

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## License

---
Copyright 2022, Thorbjørn Koch ([@Thunderl3ear](https://gitlab.com/Thunderl3ear))

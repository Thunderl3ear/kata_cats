#include <iostream>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;
#include <filesystem>

int main(int, char**) 
{
    std::string folder_name = "cats_downloaded";
    std::string mkdir = "mkdir -p "+ folder_name;
    system(mkdir.c_str());
    std::filesystem::path p1 {folder_name.c_str()};

    int count = 0;
    for (auto& p : std::filesystem::directory_iterator(p1))
    {
        ++count;
    }
    
    std::string fname = std::to_string(count)+"_cat.jpg";

    RestClient::Response r = RestClient::get("https://aws.random.cat/meow");

    auto json_body = json::parse(r.body);
    std::string cat_url = json_body["file"];
    std::string wget = "wget -O "+folder_name+"/"+fname+" "+ cat_url;
    
    std::string xdgopen = "xdg-open "+ folder_name+"/"+fname;

    system(wget.c_str());
    system(xdgopen.c_str());
}

